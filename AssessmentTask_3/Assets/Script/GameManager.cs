using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text;
using System.Linq;

public class GameManager : NetworkBehaviour
{
    public GameObject cardBoard;
    public CardDatabase db;
    public TextMeshProUGUI actionLog;
    private StringBuilder builder;
    private List<Card> resultList;

    private void Start()
    {
        builder = new StringBuilder();
        actionLog = GameObject.Find("LogContent").GetComponent<TextMeshProUGUI>();
        resultList = new List<Card>();
    }

    public override void OnStopServer()
    {
        base.OnStopServer();

        if(isServer)
        {
            ResetLog();
            resultList.Clear();
        }
    }

    public void ResetLog()
    {
        actionLog.text = string.Empty;
        builder.Clear();
    }

    public void AddLog(string log)
    {
        builder.Append($"{log}\n\n");
        actionLog.text = builder.ToString();
    }

    public void GetRandomCards()
    {
        if(resultList == null)
        {
            resultList = new List<Card>();
        }
        else if(resultList.Count >= 3)
        {
            resultList.Clear();
        }

        AddLog("Preapreing Cards");
        List<Card> tempList = db.CardList.ToList();
        

        while(resultList.Count < 3)
        {
            Card selectedCard = tempList[Random.Range(0, tempList.Count)];
            resultList.Add(selectedCard);
            tempList.Remove(selectedCard);
        }

        AddLog($"Selected {resultList[0].cardName},{resultList[1].cardName},{resultList[2].cardName}");
    }

    public Card RevealCard(int selectedIndex)
    {
        if (resultList != null && resultList.Count > 0)
        {
            Card result = resultList[selectedIndex];
            AddLog($"Card Revealed:\nCardName: {result.name}\nDescription: {result.skillDescription}");
            return result;
        }
        else return null;
    }

    
}
