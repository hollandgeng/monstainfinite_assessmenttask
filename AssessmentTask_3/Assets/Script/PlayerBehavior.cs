using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
using System.Text;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class PlayerBehavior : NetworkBehaviour
{
    [SerializeField] private GameManager gameManager;
    public GameObject cardBoard;

    private GameObject selectedCard;
    private bool isSelected = false;
    private GameObject currentBoard = null;

    public override void OnStartServer()
    {
        base.OnStartServer();

        if (isServer)
        {
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }
    }


    public override void OnStartClient()
    {
        base.OnStartClient();

        if(isClientOnly)
        {
            RegisterToServer("Player1");
        }
    }

    public override void OnStopClient()
    {
        base.OnStopClient();
        Destroy(currentBoard);
    }

    [Command]
    private void RegisterToServer(string name)
    {
        Debug.Log(name);
        gameManager.AddLog($"{name} has joined the lobby");
        gameManager.GetRandomCards();
        SpawnBoard();
    }

    [TargetRpc]
    public void SpawnBoard()
    {
        if(isLocalPlayer && currentBoard == null)
        {
            currentBoard = (GameObject)Instantiate(cardBoard);
            AssignCard();
        }
    }

    private void AssignCard()
    {
        if(currentBoard != null)
        {
            int totalcard = currentBoard.transform.GetChild(0).transform.childCount;
            for (int i = 0; i < totalcard; i++)
            {
                GameObject card01 = GameObject.Find($"Card{i}");
                if(card01 != null)
                {
                    int index = i;
                    card01.GetComponent<Button>().onClick.AddListener(()=>OnCardSelected(index));
                }

            }

        }
    }

    private void OnCardSelected(int index)
    {
        if (!isSelected)
        {
            selectedCard = EventSystem.current.currentSelectedGameObject;
            //Debug.Log(selectedCard.name);

            ReceiveSelectedCard(index);
            
            isSelected = true;
        }
    }

    [Command]
    public void ReceiveSelectedCard(int index)
    {
        gameManager.AddLog($"Player1 selected {index}");
        Card cardInfo = gameManager.RevealCard(index);
        ShowSelectedCard(cardInfo);
    }

    [TargetRpc]
    public void ShowSelectedCard(Card card)
    {
        if(card != null)
        {            
            GameObject cardFront = selectedCard.transform.GetChild(1).gameObject;
            TextMeshProUGUI desc = cardFront.transform.Find("description").GetComponent<TextMeshProUGUI>();
            desc.text = card.skillDescription;

            TextMeshProUGUI name = cardFront.transform.Find("name").GetComponent<TextMeshProUGUI>();
            name.text = card.cardName;

            Image skillIcon = cardFront.transform.Find("skillIcon").GetComponent<Image>();
            Sprite icon = Resources.Load<Sprite>(card.cardName);
            skillIcon.sprite = icon;

            cardFront.SetActive(true);
        }
        else
        {
            Debug.LogError("Card data null");
        }
    }
    

}
