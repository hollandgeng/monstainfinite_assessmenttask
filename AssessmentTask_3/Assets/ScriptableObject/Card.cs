using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName ="Card", menuName = "New Card")]
[Serializable]
public class Card : ScriptableObject
{
    public string cardName;

    //public string cardIconID;

    [TextArea(2,4)]
    public string skillDescription;
}

