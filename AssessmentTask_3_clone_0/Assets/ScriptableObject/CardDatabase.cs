using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Card Database", menuName = "New Card Database")]
public class CardDatabase : ScriptableObject
{
    public List<Card> CardList;
}
